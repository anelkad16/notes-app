package com.example.notesapp.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.notesapp.adapters.MyNoteRecyclerViewAdapter
import com.example.notesapp.data.Note
import com.example.notesapp.databinding.DialogAddNoteBinding
import com.example.notesapp.databinding.FragmentNotesListBinding
import com.example.notesapp.ui.NotesFragmentStatus.*
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import java.text.SimpleDateFormat
import java.util.Date

class NotesFragment : Fragment() {

    private val adapter = MyNoteRecyclerViewAdapter(
        onItemClick = ::onItemClick
    )
    private lateinit var binding: FragmentNotesListBinding

    private val fragmentViewModel: NotesViewModel by activityViewModels(
        factoryProducer = { NotesViewModel.factory(requireContext()) }
    )

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentNotesListBinding.inflate(layoutInflater, container, false)
        binding.floatingBtn.setOnClickListener { showAddNoteDialog() }
        binding.list.adapter = adapter
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fragmentViewModel.notesList.observe(viewLifecycleOwner) {
            adapter.updateValues(it)
        }
        fragmentViewModel.status.observe(viewLifecycleOwner) {
            fragmentViewModel.notesList.value?.let { list -> adapter.updateValues(list) }
        }
    }

    private fun onItemClick(note: Note) {
        val action = NotesFragmentDirections.actionEditNote(note.id.toInt())
        findNavController().navigate(action)
    }

    private fun showAddNoteDialog() {
        val binding = DialogAddNoteBinding.inflate(LayoutInflater.from(context))
        val view = binding.root

        MaterialAlertDialogBuilder(requireContext())
            .setTitle(ADD_NOTE_TITLE)
            .setView(view)
            .setPositiveButton(ADD_BTN) { dialog, _ ->
                fragmentViewModel.addNewNoteToFragment(
                    name = binding.name.text.toString(),
                    text = binding.text.text.toString(),
                    date = formatDate(
                        Date()
                    )
                )
                dialog.dismiss()
            }
            .setNegativeButton(CANCEL_BTN) { dialog, _ -> dialog.dismiss() }
            .show()
    }

    @SuppressLint("SimpleDateFormat")
    private fun formatDate(date: Date): String {
        val formatter = SimpleDateFormat(DATE_FORMAT)
        return formatter.format(date)
    }

    companion object {
        private const val DATE_FORMAT = "dd/MM/yyyy HH:mm:ss"
        private const val ADD_NOTE_TITLE = "Add note"
        private const val ADD_BTN = "Add"
        private const val CANCEL_BTN = "Cancel"
    }
}