package com.example.notesapp.ui

import com.example.notesapp.data.Note

sealed interface NotesFragmentStatus {
    
    data class NoteIsAdded(val newItem: Note) : NotesFragmentStatus
    object NoteIsUpdated : NotesFragmentStatus
    object NoteIsDeleted : NotesFragmentStatus
}