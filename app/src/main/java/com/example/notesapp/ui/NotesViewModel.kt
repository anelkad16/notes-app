package com.example.notesapp.ui

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.example.notesapp.data.Note
import com.example.notesapp.data.NoteDatabase
import com.example.notesapp.ui.NotesFragmentStatus.NoteIsAdded
import com.example.notesapp.ui.NotesFragmentStatus.NoteIsDeleted
import com.example.notesapp.ui.NotesFragmentStatus.NoteIsUpdated
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class NotesViewModel(
    private val database: NoteDatabase,
) : ViewModel() {

    private val _status: MutableLiveData<NotesFragmentStatus> = MutableLiveData()
    val status: LiveData<NotesFragmentStatus>
        get() = _status

    private val _noteText: MutableLiveData<String> = MutableLiveData()
    val noteText: LiveData<String> = _noteText

    private val _notesList: MutableLiveData<List<Note>> = MutableLiveData(emptyList())
    val notesList: LiveData<List<Note>> = _notesList

    init {
        refreshNotes()
    }

    fun addNewNoteToFragment(
        name: String,
        text: String,
        date: String
    ) {
        val newNote = Note(
            name = name,
            text = text,
            date = date
        )
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                database.noteDao().insertAll(newNote)
            }
            refreshNotes()
            _status.value = NoteIsAdded(newNote)
        }
    }

    fun updateNote(id: Long, newText: String) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                database.noteDao().update(id, newText)
            }
            refreshNotes()
            _status.value = NoteIsUpdated
        }
    }

    fun deleteNote(id: Long) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                database.noteDao().delete(id)
            }
            refreshNotes()
            _status.value = NoteIsDeleted
        }
    }

    fun getText(id: Long) {
        viewModelScope.launch {
            val noteText: String = withContext(Dispatchers.IO) {
                database.noteDao().getText(id)
            }
            _noteText.value = noteText
        }
    }

    private fun refreshNotes() {
        viewModelScope.launch {
            val notes = withContext(Dispatchers.IO) {
                database.noteDao().getAll()
            }
            _notesList.value = notes
        }
    }

    companion object {
        fun factory(context: Context) =
            viewModelFactory {
                initializer {
                    val database = NoteDatabase.getDatabase(context)
                    NotesViewModel(database)
                }
            }
    }
}