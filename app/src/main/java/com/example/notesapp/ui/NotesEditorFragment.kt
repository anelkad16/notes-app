package com.example.notesapp.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.notesapp.databinding.FragmentNotesEditorBinding

class NotesEditorFragment : Fragment() {

    private val viewModel: NotesViewModel by activityViewModels(
        factoryProducer = { NotesViewModel.factory(requireContext()) }
    )
    private val args: NotesEditorFragmentArgs by navArgs()
    private lateinit var binding: FragmentNotesEditorBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentNotesEditorBinding.inflate(inflater, container, false)
        viewModel.getText(args.noteId.toLong())
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindViews()

        viewModel.noteText.observe(viewLifecycleOwner) { text ->
            binding.textInput.setText(text)
            binding.noteText.text = "Note: \n$text"
        }
    }

    @SuppressLint("SetTextI18n")
    private fun bindViews() {
        binding.apply {
            textInput.setText(viewModel.noteText.toString())
            noteText.text = "Note:\n${viewModel.noteText}"
            saveButton.setOnClickListener {
                saveChanges()
            }
            deleteButton.setOnClickListener {
                viewModel.deleteNote(args.noteId.toLong())
                findNavController().navigateUp()
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun saveChanges() {
        viewModel.updateNote(
            id = args.noteId.toLong(),
            newText = binding.textInput.text.toString()
        )
        binding.noteText.text = "Note: \n${binding.textInput.text}"
        findNavController().navigateUp()
    }
}