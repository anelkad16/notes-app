package com.example.notesapp.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface NoteDao {

    @Query("SELECT * FROM note_table")
    fun getAll(): List<Note>

    @Insert
    fun insertAll(vararg notes: Note)

    @Query("DELETE FROM note_table WHERE id = :id")
    fun delete(id: Long)

    @Query("UPDATE note_table SET text = :newText WHERE id = :id")
    fun update(id: Long, newText: String)

    @Query("SELECT text FROM note_table WHERE id = :id")
    fun getText(id: Long): String
}