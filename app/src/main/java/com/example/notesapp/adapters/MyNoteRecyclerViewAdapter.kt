package com.example.notesapp.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.notesapp.data.Note
import com.example.notesapp.databinding.FragmentNotesBinding

class MyNoteRecyclerViewAdapter(
    private val onItemClick: (Note) -> Unit,
    private val values: MutableList<Note> = mutableListOf()
) : RecyclerView.Adapter<MyNoteRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            FragmentNotesBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.bind(item)
    }

    override fun getItemCount(): Int = values.size

    fun updateValues(newValues: List<Note>) {
        val calculateDiff = DiffUtil.calculateDiff(NotesListCallback(values, newValues))
        values.clear()
        values.addAll(newValues)
        calculateDiff.dispatchUpdatesTo(this)
    }

    inner class ViewHolder(private val binding: FragmentNotesBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(note: Note) = with(binding) {
            noteName.text = note.name
            noteDate.text = note.date
            root.setOnClickListener { onItemClick(note) }
        }
    }
}